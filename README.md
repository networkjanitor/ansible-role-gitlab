# ansible-role-gitlab

## Requirements

None.

## Role Variables

```
gitlab_config*: configuration data for gitlab.rb, see example rendering
gitlab_raw_config: additional raw configuration which will be included at the end of gitlab.rb

gitlab_local_ssl_certs: local path from where ssl certificates should be copied to /etc/gitlab/ssl/
gitlab_local_trusted_certs: local path from where trusted ca certificates should be copied to /etc/gitlab/trusted-certs/

gitlab_edition: gitlab-ce or gitlab-ee
gitlab_version: gitlab version which should be installed (e.g. 14.4.1-ce.0 for Debian-like, or 14.4.1-ce.0.el8 for RedHat-like distributions).
gitlab_ldap_secrets: ldap secrets string which should be saved in gitlabs encrypted variable storage



```


## Dependencies

None.

## Example Playbook

```
- hosts: all
  roles:
    - { role: networkjanitor.gitlab, tags: ['gitlab'], become: yes }
  vars:
    gitlab_edition: gitlab-ce
```

## Example gitlab.rb rendering

The following config demonstrates the capabilities of the configuration for /etc/gitlab/gitlab.rb (the settings are default/nonsensical).

All variables starting with `gitlab_config` are merged into one by the role, which allows for the same variable precedence as with individual variables. This means that there is no need to have a dedicated variable per setting (e.g. gitlab_rails_gravatar_plain_url), but rather all gitlab.rb settings are derived from the merged gitlab_config dict.

This reduces maintenance on the role, as no new configuration settings need to be added if they get introduced in gitlab.

```
$ cat host_vars/myserver.yml
gitlab_config_hostvars:
  external_url: 'https://example.com'
  roles: ['redis_sentinel_role', 'redis_master_role']
  gitlab_rails:
    gravatar_plain_url: "http://www.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon"
  patroni:
    standby_cluster:

$ cat group_vars/mygroup/vars.yml
gitlab_config_groupvars:
  registry:
    default_notifications_timeout: "500ms"
  gitlab_workhorse:
    image_scaler_max_filesize: 250000
    consul_service_meta: |-
        {}
  gitlab_shell:
    http_settings: |-
        { user: 'username', password: 'password', ca_file: '/etc/ssl/cert.pem', ca_path: '/etc/pki/tls/certs', self_signed_cert: false}
    log_format: |-
        'json'
    log_directory: |-
        "/var/log/gitlab/gitlab-shell/"
  sidekiq:
    routing_rules: []
    queue_groups: ['*']
  patroni:
    standby_cluster:
      enable: false
      host: nil
      port: 5432
  gitlab_rails:
    object_store:
      proxy_download: false
      artifacts:
        bucket: nil
    omniauth_providers: |-
            [{
                "name" => "google_oauth2",
                "app_id" => "YOUR APP ID",
                "app_secret" => "YOUR APP SECRET",
                "args" => { "access_type" => "offline", "approval_prompt" => "" }
            }]
    ldap_servers: |-
            YAML.load <<-'EOS'
              main: # 'main' is the GitLab 'provider ID' of this LDAP server
                  label: 'LDAP'
                  host: '_your_ldap_server'
                  port: 389
                  uid: 'sAMAccountName'
                  bind_dn: '_the_full_dn_of_the_user_you_will_bind_with'
                  password: '_the_password_of_the_bind_user'
                  encryption: 'plain' # "start_tls" or "simple_tls" or "plain"
                  verify_certificates: true
                  smartcard_auth: false
                  active_directory: true
                  allow_username_or_email_login: false
                  lowercase_usernames: false
                  block_auto_created_users: false
                  base: ''
                  user_filter: ''
                  ## EE only
                  group_base: ''
                  admin_group: ''
                  sync_ssh_keys: false
            EOS
gitlab_raw_config: |-
  git_data_dirs({
    "default" => { "path" => "/var/opt/gitlab/git-data" },
    "alternative" => { "path" => "/mnt/nas/git-data" }
  })

```

Will render the following config on the server:

```
$ cat /etc/gitlab/gitlab.rb
external_url 'https://example.com'
gitlab_rails['gravatar_plain_url'] = 'http://www.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon'
gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
  main: # 'main' is the GitLab 'provider ID' of this LDAP server
      label: 'LDAP'
      host: '_your_ldap_server'
      port: 389
      uid: 'sAMAccountName'
      bind_dn: '_the_full_dn_of_the_user_you_will_bind_with'
      password: '_the_password_of_the_bind_user'
      encryption: 'plain' # "start_tls" or "simple_tls" or "plain"
      verify_certificates: true
      smartcard_auth: false
      active_directory: true
      allow_username_or_email_login: false
      lowercase_usernames: false
      block_auto_created_users: false
      base: ''
      user_filter: ''
      ## EE only
      group_base: ''
      admin_group: ''
      sync_ssh_keys: false
EOS
gitlab_rails['object_store']['artifacts']['bucket'] = nil
gitlab_rails['object_store']['proxy_download'] = false
gitlab_rails['omniauth_providers'] = [{
    "name" => "google_oauth2",
    "app_id" => "YOUR APP ID",
    "app_secret" => "YOUR APP SECRET",
    "args" => { "access_type" => "offline", "approval_prompt" => "" }
}]
gitlab_shell['http_settings'] = { user: 'username', password: 'password', ca_file: '/etc/ssl/cert.pem', ca_path: '/etc/pki/tls/certs', self_signed_cert: false}
gitlab_shell['log_directory'] = "/var/log/gitlab/gitlab-shell/"
gitlab_shell['log_format'] = 'json'
gitlab_workhorse['consul_service_meta'] = {}
gitlab_workhorse['image_scaler_max_filesize'] = 250000
registry['default_notifications_timeout'] = '500ms'
roles ['redis_sentinel_role', 'redis_master_role']
sidekiq['queue_groups'] = ['*']
sidekiq['routing_rules'] = []

git_data_dirs({
  "default" => { "path" => "/var/opt/gitlab/git-data" },
  "alternative" => { "path" => "/mnt/nas/git-data" }
})
```
